#pragma once


// Screen
#define SCRN_WIDTH 1000
#define SCRN_HEIGHT 800


// Splash
#define SPLASH_STATE_SHOW_TIME 5000


// Main Menu
#define MAIN_PLAY PLAY_BTN


// Game
#define GM_O_PIECE "O"
#define GM_X_PIECE "X"
#define GM_O_WIN "OWin"
#define GM_X_WIN "XWin"
#define GM_DRAW "Draw"

#define GM_PAUSE PAUSE_BTN

#define GM_GRID_MINI "GameMini"
#define GM_GRID_SUPER "GameSuper"

#define GM_VIEW "View"
#define GM_PLAY "Play"


// Game Over
#define OVER_RETRY "OverRetry"
#define OVER_HOME HOME_BTN
#define OVER_X_WIN "OverXWin"
#define OVER_O_WIN "OverOWin"
#define OVER_DRAW "OverDraw"


// Pause
#define PAUSE_RESUME PLAY_BTN
#define PAUSE_HOME HOME_BTN


// Misc
#define LOGO "Logo"
#define HOME_BTN "HomeBtn"
#define PAUSE_BTN "PauseBtn"
#define PLAY_BTN "PlayBtn"
#define TIME_PLAY 1000
#define TIME_VIEW 800
#define STATE_O_WIN 10
#define STATE_X_WIN 11
#define STATE_DRAW 12


// Piece values
#define X_VALUE 1
#define O_VALUE 2
#define EMPTY_VALUE 0


// Fonts
#define LOGO_FONT "AldoTheApache"
