#pragma once

#include <SFML/Graphics.hpp>
#include "Game.hpp"
#include "State.hpp"

namespace Supergalo{
    class GameOverState : public State{
    public:
        GameOverState(GameDataRef data);
        GameOverState(GameDataRef data, int gameState);

        void init();
        void handleInput();
        void update();
        void draw();

    private:
        GameDataRef data;
        int gameState;

        sf::Sprite retryBtn;
        sf::Sprite homeBtn;
        sf::Sprite state;
        sf::Color background;
    };
}
