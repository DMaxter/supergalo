#pragma once

#include <SFML/Graphics.hpp>
#include <vector>
#include "State.hpp"
#include "Game.hpp"

namespace Supergalo{
    class GameState : public State{
    public:
        GameState(GameDataRef data);
        ~GameState();

        void init();
        void handleInput();
        void update();
        void draw();
        int getState();

    private:
        // Mini Grid class
        class Grid{
        public:
            Grid(GameDataRef data, sf::Vector2f bounds, std::string texture);
            bool place(int piece, int x, int y, std::string texture);
            const sf::Texture *getTexture(int x, int y);
            void setTexture(int x, int y, const sf::Texture *texture);
            void draw();
            bool clicked(int x, int y);
            bool win(int x, int y);
            void setFull();
            bool isFull();
        private:
            GameDataRef data;
            bool full;
            int grid[3][3];
            sf::Sprite gridPieces[3][3];
        };

        GameDataRef data;

        sf::Sprite pauseBtn;
        sf::Clock playClk;
        sf::Clock viewClk;

        // Mini grid (right bottom corner)
        Grid *miniGrid;

        // Currently viewing grid
        Grid *view;
        int viewX, viewY;
        const sf::Texture *viewBack;

        // Grid to place next move
        Grid *play;
        int playX, playY;
        const sf::Texture *playBack;
        bool played;

        // Mini grids
        std::vector<std::vector<Grid>> gridArray;

        int turn;

        // CLICK FLAGS
        int minX, minY, supX, supY;
        bool pauseClick;
    };

}
