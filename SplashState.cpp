#include "SplashState.hpp"
#include "MainMenuState.hpp"
#include "reference.hpp"

namespace Supergalo{
    SplashState::SplashState(GameDataRef data){
        this->data = data;
    }

    void SplashState::init(){
        this->logo.setTexture(this->data->assets.getTexture(LOGO));

        this->logo.setOrigin(this->logo.getLocalBounds().width * 0.5, this->logo.getLocalBounds().height * 0.5);

        this->logo.setPosition(SCRN_WIDTH / 2, SCRN_HEIGHT / 3);

        this->title.setFont(this->data->assets.getFont(LOGO_FONT));
        this->title.setString("SuperGalo");
        this->title.setCharacterSize(200);
        this->title.setFillColor(sf::Color::White);

        this->title.setOrigin(0.5 * this->title.getLocalBounds().width, 0.5 * this->title.getLocalBounds().height);
        this->title.setPosition(SCRN_WIDTH / 2, SCRN_HEIGHT * 2 / 3);
    }

    void SplashState::handleInput(){
        sf::Event event;

        while(this->data->window.pollEvent(event)){
            if(sf::Event::Closed == event.type){
                this->data->window.close();
            }
        }
    }

    void SplashState::update(){
        if(this->clock.getElapsedTime().asMilliseconds() > SPLASH_STATE_SHOW_TIME){
            this->data->machine.addState(StateRef(new MainMenuState(this->data)), 1);
        }
    }

    void SplashState::draw(){
        this->data->window.clear(sf::Color(255, 110, 74));

        this->data->window.draw(this->logo);
        this->data->window.draw(this->title);

        this->data->window.display();
    }
}
