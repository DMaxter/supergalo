# Super Galo

Supergalo is a variant of the well-known game Tic-Tac-Toe, where each of the spaces of the traditional grid is another grid.

## Installation

### Requirements

The only things you will need are:
- a C++ compiler (GNU C Compiler - gcc - is the only one tested)
- SFML (the graphical library used)

### Build

To build this game all you have to do is:

```
make
```

## Usage

To run this game is as simple as running (in the building directory):

```
./Supergalo
```

## Contributing

This project is open to sugestions (using the Issues) and Merge Requests with proper modifications.

By reporting bugs/problems you are also contributing to this project.


## License

This project is licensed under [GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
