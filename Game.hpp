#pragma once

#include <memory>
#include <string>
#include <SFML/Graphics.hpp>
#include "StateMachine.hpp"
#include "AssetManager.hpp"
#include "InputManager.hpp"

namespace Supergalo{
    struct GameData{
        StateMachine machine;
        sf::RenderWindow window;
        AssetManager assets;
        InputManager input;
    };

    typedef std::shared_ptr<GameData> GameDataRef;

    class Game{
    public:
        Game(int width, int height, std::string title);

    private:
        GameDataRef data = std::make_shared<GameData>();

        void run();

        void loadTextures(GameDataRef &data);
    };
}
