#pragma once

// Splash State
#define GAME_LOGO "resources/images/logo.png"
#define SPLASH_STATE_BACKGROUND "resources/images/splashBG.jpeg"


// Main State
#define MAIN_MENU_PLAY_BUTTON "resources/images/playBtn.png"


// Game State
#define GAME_PAUSE "resources/images/pauseBtn.png"

#define GAME_X_PIECE "resources/images/X.png"
#define GAME_O_PIECE "resources/images/O.png"

#define GAME_X_WIN "resources/images/Xwin.png"
#define GAME_O_WIN "resources/images/Owin.png"
#define GAME_DRAW "resources/images/draw.png"

#define GRID_BIG "resources/images/grid.png"
#define GRID_MINI "resources/images/miniGrid.png"

#define SHOW_VIEW "resources/images/view.png"
#define SHOW_PLAY "resources/images/play.png"


// Game Over State
#define RETRY_RETRY_BUTTON "resources/images/retryBtn.png"
#define RETRY_HOME_BUTTON "resources/images/backBtn.png"
#define RETRY_X_WIN "resources/images/gameXWin.png"
#define RETRY_O_WIN "resources/images/gameOWin.png"
#define RETRY_DRAW "resources/images/gameDraw.png"


// Fonts
#define ALDO_APACHE_FONT "resources/fonts/AldotheApache.ttf"
