# Add -O3
OPTIONS=-Wall -Wextra -lsfml-graphics -lsfml-window -lsfml-system -ggdb3

all: main.o AssetManager.o InputManager.o StateMachine.o Game.o SplashState.o MainMenuState.o GameState.o PauseState.o GameOverState.o
	g++ ${OPTIONS} -o Supergalo main.o AssetManager.o InputManager.o StateMachine.o Game.o SplashState.o MainMenuState.o GameState.o PauseState.o GameOverState.o

clean:
	rm *.o Supergalo

main.o: main.cpp Game.hpp reference.hpp
	g++ ${OPTIONS} -c main.cpp

AssetManager.o: AssetManager.cpp AssetManager.hpp
	g++ ${OPTIONS} -c AssetManager.cpp

InputManager.o: InputManager.cpp InputManager.hpp
	g++ ${OPTIONS} -c InputManager.cpp

StateMachine.o: StateMachine.cpp StateMachine.hpp
	g++ ${OPTIONS} -c StateMachine.cpp

Game.o: Game.cpp Game.hpp StateMachine.hpp AssetManager.hpp InputManager.hpp reference.hpp textures.hpp
	g++ ${OPTIONS} -c Game.cpp

SplashState.o: SplashState.cpp SplashState.hpp MainMenuState.hpp reference.hpp
	g++ ${OPTIONS} -c SplashState.cpp

MainMenuState.o: MainMenuState.cpp MainMenuState.hpp GameState.hpp reference.hpp
	g++ ${OPTIONS} -c MainMenuState.cpp

GameState.o: GameState.cpp GameState.cpp PauseState.hpp reference.hpp
	g++ ${OPTIONS} -c GameState.cpp

PauseState.o: PauseState.cpp PauseState.hpp MainMenuState.hpp GameState.hpp reference.hpp
	g++ ${OPTIONS} -c PauseState.cpp

GameOverState.o: GameOverState.cpp GameOverState.hpp MainMenuState.hpp GameState.hpp reference.hpp
	g++ ${OPTIONS} -c GameOverState.cpp
