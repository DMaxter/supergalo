#include "Game.hpp"
#include "SplashState.hpp"
#include "reference.hpp"
#include "textures.hpp"

namespace Supergalo{
    Game::Game(int width, int height, std::string title){
        this->data->window.create(sf::VideoMode(width, height), title, sf::Style::Close | sf::Style::Titlebar);
        this->data->window.setFramerateLimit(30);

        this->data->machine.addState(StateRef(new SplashState(this->data)));

        this->loadTextures(this->data);

        this->run();
    }

    void Game::run(){
        // Game cycle
        while(this->data->window.isOpen()){
            this->data->machine.processStateChanges();
            this->data->machine.getActiveState()->handleInput();
            this->data->machine.getActiveState()->update();
            this->data->machine.getActiveState()->draw();
        }
    }

    void Game::loadTextures(GameDataRef &data){
        AssetManager &assets = data->assets;

        // Load fonts
        assets.loadFont(LOGO_FONT, ALDO_APACHE_FONT);

        // Load logo
        assets.loadTexture(LOGO, GAME_LOGO);

        // Load main
        assets.loadTexture(MAIN_PLAY, MAIN_MENU_PLAY_BUTTON);

        // Load game
        assets.loadTexture(GM_X_PIECE, GAME_X_PIECE);
        assets.loadTexture(GM_O_PIECE, GAME_O_PIECE);
        assets.loadTexture(GM_X_WIN, GAME_X_WIN);
        assets.loadTexture(GM_O_WIN, GAME_O_WIN);
        assets.loadTexture(GM_DRAW, GAME_DRAW);
        assets.loadTexture(GM_PAUSE, GAME_PAUSE);
        assets.loadTexture(GM_GRID_MINI, GRID_MINI);
        assets.loadTexture(GM_GRID_SUPER, GRID_BIG);
        assets.loadTexture(GM_VIEW, SHOW_VIEW);
        assets.loadTexture(GM_PLAY, SHOW_PLAY);

        // Load game over
        assets.loadTexture(OVER_RETRY, RETRY_RETRY_BUTTON);
        assets.loadTexture(OVER_HOME, RETRY_HOME_BUTTON);
        assets.loadTexture(OVER_X_WIN, RETRY_X_WIN);
        assets.loadTexture(OVER_O_WIN, RETRY_O_WIN);
        assets.loadTexture(OVER_DRAW, RETRY_DRAW);
    }
}
