#include "StateMachine.hpp"

namespace Supergalo{
    void StateMachine::addState(StateRef newState, unsigned int replace){
        this->isAdding = true;
        this->replace = replace;

        this->newState = std::move(newState);
    }

    void StateMachine::removeState(){
        this->isRemoving = true;
    }

    void StateMachine::processStateChanges(){
        // Check if is the last state
        if(this->isRemoving && !(this->states.empty())){
            this->states.pop_back();

            if(!(this->states.empty())){
                this->states.back()->resume();
            }

            this->isRemoving = false;
        }

        // And state and check for replacing
        if(this->isAdding){
            if(!(this->states.empty())){
                if(this->replace){
                    while(this->replace--){
                        this->states.pop_back();
                    }
                }else{
                    this->states.back()->pause();
                }
            }

            this->states.push_back(std::move(this->newState));
            this->states.back()->init();
            this->isAdding = false;
        }
    }

    StateRef &StateMachine::getActiveState(){
        return this->states.back();
    }

    StateRef &StateMachine::getState(unsigned int x){
        return this->states[this->states.size() - 1 - x];
    }
}
