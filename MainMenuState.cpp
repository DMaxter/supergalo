#include <sstream>
#include "MainMenuState.hpp"
#include "GameState.hpp"
#include "reference.hpp"

namespace Supergalo{
    MainMenuState::MainMenuState(GameDataRef data){
        this->data = data;
    }

    void MainMenuState::init(){
        this->playBtn.setTexture(this->data->assets.getTexture(MAIN_PLAY));

        this->playBtn.setOrigin(0.5 * this->playBtn.getLocalBounds().width, 0.5 * this->playBtn.getLocalBounds().height);
        this->playBtn.setPosition(SCRN_WIDTH / 2, SCRN_HEIGHT * 2 / 3);


        this->title.setFont(this->data->assets.getFont(LOGO_FONT));
        this->title.setString("SuperGalo");
        this->title.setCharacterSize(200);
        this->title.setFillColor(sf::Color::White);

        this->title.setOrigin(0.5 * this->title.getLocalBounds().width, 0.5 * this->title.getLocalBounds().height);
        this->title.setPosition(SCRN_WIDTH / 2, SCRN_HEIGHT / 6);
    }

    void MainMenuState::handleInput(){
        sf::Event event;

        while(this->data->window.pollEvent(event)){
            if(sf::Event::Closed == event.type){
                this->data->window.close();
            }

            if(this->data->input.isSpriteClicked(this->playBtn, sf::Mouse::Left, this->data->window)){
                this->data->machine.addState(StateRef(new GameState(this->data)), 1);
            }
        }
    }

    void MainMenuState::update(){

    }

    void MainMenuState::draw(){
        this->data->window.clear(sf::Color(59,169,255));

        this->data->window.draw(this->title);
        this->data->window.draw(this->playBtn);

        this->data->window.display();
    }
}
