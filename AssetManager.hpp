#pragma once

#include <SFML/Graphics.hpp>
#include <map>

// Save all assets of the game

namespace Supergalo{
    class AssetManager{
    public:
        AssetManager() {}
        ~AssetManager() {}

        // Texture handling
        void loadTexture(std::string name, std::string filename);
        sf::Texture &getTexture(std::string name);

        // Font handling
        void loadFont(std::string name, std::string filename);
        sf::Font &getFont(std::string name);

    private:
        std::map<std::string, sf::Texture> textures;
        std::map<std::string, sf::Font> fonts;
    };
}
