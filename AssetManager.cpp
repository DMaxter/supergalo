#include "AssetManager.hpp"

namespace Supergalo{
    // Textures
    void AssetManager::loadTexture(std::string name, std::string filename){
        sf::Texture tex;

        if(this->textures.find(name) == this->textures.end() && tex.loadFromFile(filename)){
            tex.setSmooth(true);
            this->textures[name] = tex;
        }
    }

    sf::Texture &AssetManager::getTexture(std::string name){
        return this->textures.at(name);
    }

    // Fonts
    void AssetManager::loadFont(std::string name, std::string filename){
        sf::Font font;

        if(this->fonts.find(name) == this->fonts.end() && font.loadFromFile(filename)){
            this->fonts[name] = font;
        }
    }

    sf::Font &AssetManager::getFont(std::string name){
        return this->fonts.at(name);
    }
}
