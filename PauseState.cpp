#include <sstream>
#include "PauseState.hpp"
#include "GameState.hpp"
#include "MainMenuState.hpp"
#include "reference.hpp"

namespace Supergalo{
    PauseState::PauseState(GameDataRef data){
        this->data = data;
    }

    void PauseState::init(){
        this->resumeBtn.setTexture(this->data->assets.getTexture(PAUSE_RESUME));
        this->homeBtn.setTexture(this->data->assets.getTexture(PAUSE_HOME));

        this->resumeBtn.setPosition((this->data->window.getSize().x / 2) - (this->resumeBtn.getLocalBounds().width / 2), (this->data->window.getSize().y / 3) - (this->resumeBtn.getLocalBounds().height / 2));
        this->homeBtn.setPosition((this->data->window.getSize().x / 2) - (this->homeBtn.getLocalBounds().width / 2), (this->data->window.getSize().y * 2 / 3) - (this->homeBtn.getLocalBounds().height / 2));
    }

    void PauseState::handleInput(){
        sf::Event event;

        while(this->data->window.pollEvent(event)){
            if(sf::Event::Closed == event.type){
                this->data->window.close();
            }

            if(this->data->input.isSpriteClicked(this->resumeBtn, sf::Mouse::Left, this->data->window)){
                this->data->machine.removeState();
            }else if(this->data->input.isSpriteClicked(this->homeBtn, sf::Mouse::Left, this->data->window)){
                this->data->machine.addState(StateRef(new MainMenuState(this->data)), 2);
            }
        }
    }

    void PauseState::update(){

    }

    void PauseState::draw(){
        this->data->window.clear(sf::Color(255, 224, 87));

        this->data->window.draw(this->resumeBtn);
        this->data->window.draw(this->homeBtn);

        this->data->window.display();
    }
}
