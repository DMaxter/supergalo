#pragma once

#include <memory>
#include <vector>

#include "State.hpp"

namespace Supergalo{
    typedef std::unique_ptr<State> StateRef;

    class StateMachine{
    public:
        StateMachine(){}
        ~StateMachine(){}

        void addState(StateRef newState, unsigned int replace = 0);
        void removeState();

        void processStateChanges();

        StateRef& getActiveState();
        StateRef& getState(unsigned int i);
    private:
        std::vector<StateRef> states;
        StateRef newState;
        bool isRemoving;
        bool isAdding;
        int replace;
    };
}
