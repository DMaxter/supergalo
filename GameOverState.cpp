#include <sstream>
#include "GameOverState.hpp"
#include "GameState.hpp"
#include "MainMenuState.hpp"
#include "reference.hpp"

namespace Supergalo{
    GameOverState::GameOverState(GameDataRef data){
        this->data = data;
    }

    GameOverState::GameOverState(GameDataRef data, int gameState){
        this->data = data;
        this->gameState = gameState;
    }

    void GameOverState::init(){
        // Set images
        this->retryBtn.setTexture(this->data->assets.getTexture(OVER_RETRY));
        this->homeBtn.setTexture(this->data->assets.getTexture(OVER_HOME));

        // Set origin
        this->retryBtn.setOrigin(0.5 * this->retryBtn.getLocalBounds().width, 0.5 * this->retryBtn.getLocalBounds().height);
        this->homeBtn.setOrigin(0.5 * this->homeBtn.getLocalBounds().width, 0.5 * this->homeBtn.getLocalBounds().height);

        // Set positions
        this->retryBtn.setPosition(this->data->window.getSize().x / 2, this->data->window.getSize().y * 2 / 4);
        this->homeBtn.setPosition(this->data->window.getSize().x / 2, this->data->window.getSize().y * 3 / 4);

        // Background color
        this->background.r = 74;
        this->background.g = 255;
        this->background.b = 87;

        if(this->gameState == STATE_X_WIN){
            this->state.setTexture(this->data->assets.getTexture(OVER_X_WIN));
        }else if(this->gameState == STATE_O_WIN){
            this->state.setTexture(this->data->assets.getTexture(OVER_O_WIN));
        }else{
            this->state.setTexture(this->data->assets.getTexture(OVER_DRAW));

            this->background.r = 177;
            this->background.g = 74;
            this->background.b = 255;
        }

        this->state.setOrigin(0.5 * this->state.getLocalBounds().width, 0.5 * this->state.getLocalBounds().height);
        this->state.setPosition(this->data->window.getSize().x / 2, this->data->window.getSize().y / 4);
    }

    void GameOverState::handleInput(){
        sf::Event event;

        while(this->data->window.pollEvent(event)){
            if(sf::Event::Closed == event.type){
                this->data->window.close();
            }

            if(this->data->input.isSpriteClicked(this->retryBtn, sf::Mouse::Left, this->data->window)){
                this->data->machine.addState(StateRef(new GameState(this->data)), 2);
            }else if(this->data->input.isSpriteClicked(this->homeBtn, sf::Mouse::Left, this->data->window)){
                this->data->machine.addState(StateRef(new MainMenuState(this->data)), 2);
            }
        }
    }

    void GameOverState::update(){

    }

    void GameOverState::draw(){
        this->data->window.clear(this->background);

        this->data->window.draw(this->state);
        this->data->window.draw(this->retryBtn);
        this->data->window.draw(this->homeBtn);

        this->data->window.display();
    }
}
