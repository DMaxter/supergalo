#pragma once

#include <SFML/Graphics.hpp>
#include "State.hpp"
#include "Game.hpp"

namespace Supergalo{
	class PauseState : public State{
		public:
			PauseState(GameDataRef data);

			void init();
			void handleInput();
			void update();
			void draw();
		private:
			GameDataRef data;
			sf::Sprite resumeBtn;
			sf::Sprite homeBtn;
	};
}
