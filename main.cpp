#include <iostream>
#include "reference.hpp"
#include "Game.hpp"

int main(){
    // Create Game instance
    Supergalo::Game(SCRN_WIDTH, SCRN_HEIGHT, "Supergalo");

    return EXIT_SUCCESS;
}
