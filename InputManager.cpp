#include "InputManager.hpp"

namespace Supergalo{
    bool InputManager::isSpriteClicked(sf::Sprite object, sf::Mouse::Button button, sf::RenderWindow &window){
        if(sf::Mouse::isButtonPressed(button)){
            return object.getGlobalBounds().contains(window.mapPixelToCoords(sf::Mouse::getPosition(window)));
        }

        return false;
    }

    sf::Vector2i InputManager::getMousePosition(sf::RenderWindow &window){
        return sf::Mouse::getPosition(window);
    }
}
