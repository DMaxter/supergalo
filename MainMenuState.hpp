#pragma once

#include <SFML/Graphics.hpp>
#include "Game.hpp"
#include "State.hpp"

namespace Supergalo{
    class MainMenuState : public State{
    public:
        MainMenuState(GameDataRef data);

        void init();
        void handleInput();
        void update();
        void draw();
    private:
        GameDataRef data;

        sf::Sprite playBtn;
        sf::Text title;
    };
}
