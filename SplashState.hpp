#pragma once

#include <SFML/Graphics.hpp>
#include "State.hpp"
#include "Game.hpp"

namespace Supergalo{
    class SplashState : public State{
    public:
        SplashState(GameDataRef data);

        void init();
        void handleInput();
        void update();
        void draw();
    private:
        GameDataRef data;

        sf::Clock clock;
        sf::Sprite logo;
        sf::Text title;
    };
}
