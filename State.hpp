#pragma once

// State interface

namespace Supergalo {
    class State{
    public:
        // Initialization
        virtual void init() = 0;

        // Handle inputs of current state
        virtual void handleInput() = 0;
        // Update variables of current state
        virtual void update() = 0;
        // Draw state to the screen
        virtual void draw() = 0;

        virtual void pause() {}
        virtual void resume() {}
    };
}
