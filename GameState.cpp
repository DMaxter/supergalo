#include <climits>
#include "GameState.hpp"
#include "GameOverState.hpp"
#include "PauseState.hpp"
#include "reference.hpp"

#define MIN_SPACING 10

namespace Supergalo{
    // Create MiniGrid
    GameState::Grid::Grid(GameDataRef data, sf::Vector2f center, std::string texture){
        this->data = data;
        this->full = false;

        sf::Texture& fill = this->data->assets.getTexture(texture);
        sf::Vector2u pieceSize = fill.getSize();
        float halfHeight = pieceSize.y * 0.5;
        float halfWidth = pieceSize.x * 0.5;

        for(int x = 0; x < 3; x++){
            for(int y = 0; y < 3; y++){
                // Set piece
                this->grid[x][y] = EMPTY_VALUE;

                // Set position and texture
                this->gridPieces[x][y].setTexture(fill);
                this->gridPieces[x][y].setOrigin(halfWidth, halfHeight);
                this->gridPieces[x][y].setPosition(center.x + (int) (pieceSize.x + MIN_SPACING) * (y - 1), center.y + (int) (pieceSize.y + MIN_SPACING) * (x - 1));
            }
        }
    }

    // Put piece on board
    bool GameState::Grid::place(int piece, int x, int y, std::string texture=""){
        if(this->grid[x][y] != EMPTY_VALUE){
            return false;
        }

        this->grid[x][y] = piece;

        if(texture == ""){
            if(piece == X_VALUE){
                this->gridPieces[x][y].setTexture(this->data->assets.getTexture(GM_X_PIECE));
            }else{
                this->gridPieces[x][y].setTexture(this->data->assets.getTexture(GM_O_PIECE));
            }
        }else{
            this->gridPieces[x][y].setTexture(this->data->assets.getTexture(texture));
        }

        return true;
    }

    const sf::Texture *GameState::Grid::getTexture(int x, int y){
        return this->gridPieces[x][y].getTexture();
    }

    void GameState::Grid::setTexture(int x, int y, const sf::Texture *texture){
        this->gridPieces[x][y].setTexture(*texture);
    }

    // Draw MiniGrid
    void GameState::Grid::draw(){
        for(int x = 0; x < 3; x++){
            for(int y = 0; y < 3; y++){
                this->data->window.draw(this->gridPieces[x][y]);
            }
        }
    }

    bool GameState::Grid::clicked(int x, int y){
        return this->data->input.isSpriteClicked(this->gridPieces[x][y], sf::Mouse::Left, this->data->window);
    }

    bool GameState::Grid::win(int x, int y){
        int x1 = (x + 1) % 3;
        int x2 = (x + 2) % 3;
        int y1 = (y + 1) % 3;
        int y2 = (y + 2) % 3;

        if(this->grid[x][y] == EMPTY_VALUE){
            return false;
        }else if((this->grid[x][y] == this->grid[x1][y] && this->grid[x][y] == this->grid[x2][y]) ||
                 (this->grid[x][y] == this->grid[x][y1] && this->grid[x][y] == this->grid[x][y2])){
            return true;
        }else{
            return ((this->grid[0][2] != EMPTY_VALUE && this->grid[0][2] == this->grid[1][1] &&
                     this->grid[1][1] == this->grid[2][0]) ||
                    (this->grid[0][0] != EMPTY_VALUE && this->grid[0][0] == this->grid[1][1] &&
                     this->grid[1][1] == this->grid[2][2]));
        }
    }

    void GameState::Grid::setFull(){
        this->full = true;
    }

    bool GameState::Grid::isFull(){
        if(!this->full){
            for(int x = 0; x < 3; x++){
                for(int y = 0; y < 3; y++){
                    if(this->grid[x][y] == EMPTY_VALUE){
                        return false;
                    }
                }
            }

            this->full = true;
        }

        return this->full;
    }

    GameState::GameState(GameDataRef data){
        std::vector<Grid> *tmp;

        this->minX = this->minY = 1;
        this->supX = this->supY = INT_MAX;
        this->pauseClick = false;
        this->data = data;

        // Create SuperGrid
        for(int x = 0; x < 3; x++){
            // Create 3 MiniGrid (1 SuperGrid line)
            tmp = new std::vector<Grid>();

            for(int y = 0; y < 3; y++){
                tmp->emplace_back(this->data, sf::Vector2f(SCRN_WIDTH * 4 / 10, SCRN_HEIGHT / 2), GM_GRID_SUPER);
            }

            this->gridArray.push_back(*tmp);
        }

        this->miniGrid = new Grid(this->data, sf::Vector2f(SCRN_WIDTH * 7 / 8, SCRN_HEIGHT * 5 / 6), GM_GRID_MINI);

        this->played = false;
    }

    GameState::~GameState(){
        // Delete SuperGrid
        for(int x = 0; x < 3; x++){
            for(int y = 0; y < 3; y++){
                Grid &tmp = this->gridArray[x].back();
                delete &tmp;
            }

            this->gridArray[x].clear();
            delete &this->gridArray[x];
        }

        this->gridArray.clear();
    }

    void GameState::init(){
        // Set view in middle and play anywhere
        this->view = &(this->gridArray[1][1]);
        this->viewX = this->viewY = 1;
        this->play = NULL;
        this->playX = this->playY = 1;

        this->playBack = this->viewBack = &(this->data->assets.getTexture(GM_GRID_MINI));

        // 'O' starts first
        this->turn = O_VALUE;

        this->pauseBtn.setTexture(this->data->assets.getTexture(GM_PAUSE));

        this->pauseBtn.setOrigin(0.5 * this->pauseBtn.getLocalBounds().width, 0.5 * this->pauseBtn.getLocalBounds().height);
        this->pauseBtn.setPosition(this->data->window.getSize().x * 7 / 8, this->data->window.getSize().y / 6);

        this->playClk.restart();

    }

    void GameState::handleInput(){
        sf::Event event;

        while(this->data->window.pollEvent(event)){
            if(sf::Event::Closed == event.type){
                this->data->window.close();
            }

            // Pause Button
            if(this->data->input.isSpriteClicked(this->pauseBtn, sf::Mouse::Left, this->data->window)){
                this->pauseClick = true;
            }

            // Check grid clicks
            for(int x = 0; x < 3; x++){
                for(int y = 0; y < 3; y++){
                    // Mini grid click
                    if(this->miniGrid->clicked(x, y)){
                        this->minX = x;
                        this->minY = y;
                        this->played = false;

                        if(this->play == NULL){
                            this->playX = x;
                            this->playY = y;
                        }

                        x = y = 2;

                        // Super grid click
                        // Check if clicked on view with NULL play grid
                    }else if(this->play == NULL){
                        if(this->view->clicked(x, y) && !this->view->isFull()){
                            this->play = this->view;
                            this->supX = x;
                            this->supY = y;

                            x = y = 2;
                        }else{
                            continue;
                        }

                        // Play not NULL
                    }else if(this->play->clicked(x, y) && this->play == this->view){
                        this->supX = x;
                        this->supY = y;

                        x = y = 2;
                    }
                }
            }
        }
    }

    void GameState::update(){
        if(this->pauseClick){
            this->data->machine.addState(StateRef(new PauseState(this->data)));

            this->pauseClick = false;

            // Super grid
        }else if(supX != INT_MAX){
            // Timeout to avoid unwanted click
            if(this->playClk.getElapsedTime().asMilliseconds() >= TIME_PLAY){
                if(!this->play->isFull() && this->play->place(this->turn, this->supX, this->supY)){
                    // Check win because of last placed piece
                    if(this->play->win(this->supX, this->supY)){
                        if(this->turn == O_VALUE){
                            this->miniGrid->place(this->turn, this->playX, this->playY, GM_O_WIN);
                        }else{
                            this->miniGrid->place(this->turn, this->playX, this->playY, GM_X_WIN);
                        }

                        this->played = true;

                        // Check end of game
                        if(this->miniGrid->win(this->playX, this->playY)){
                            if(this->turn == O_VALUE){
                                this->data->machine.addState(StateRef(new GameOverState(this->data, STATE_O_WIN)));
                            }else{
                                this->data->machine.addState(StateRef(new GameOverState(this->data, STATE_X_WIN)));
                            }

                            // Check for draw
                        }else if(this->miniGrid->isFull()){
                            this->data->machine.addState(StateRef(new GameOverState(this->data, STATE_DRAW)));
                        }

                        this->play->setFull();
                    }else if(this->play->isFull()){
                        this->play = NULL;
                        this->miniGrid->place(this->turn, this->playX, this->playY, GM_DRAW);
                    }

                    this->turn = (this->turn) % O_VALUE + X_VALUE;
                    this->play = &(this->gridArray[this->supX][this->supY]);
                    this->playX = this->supX;
                    this->playY = this->supY;

                    // Check if next grid to play has no wins
                    if(this->play->isFull()){
                        this->playX = this->playY = 0;
                        this->play = NULL;
                    }

                    this->minX = this->supX;
                    this->minY = this->supY;

                    this->viewClk.restart();
                }

                this->playClk.restart();
            }

            this->supX = this->supY = INT_MAX;
        }

        // Mini Grid
        if(minX != INT_MAX && this->viewClk.getElapsedTime().asMilliseconds() >= TIME_VIEW){
            this->view = &(this->gridArray[this->minX][this->minY]);

            if(this->played || this->viewX != this->minX || this->viewY != this->minY){
                if(!played){
                    this->miniGrid->setTexture(this->viewX, this->viewY, this->viewBack);

                    this->viewX = this->minX;
                    this->viewY = this->minY;
                }else{
                    this->played = false;
                }

                this->viewBack = this->miniGrid->getTexture(this->viewX, this->viewY);
            }

            this->miniGrid->setTexture(this->viewX, this->viewY, &(this->data->assets.getTexture(GM_VIEW)));

            this->minX = this->minY = INT_MAX;

            this->viewClk.restart();
        }
    }

    void GameState::draw(){
        this->data->window.clear(sf::Color(255, 224, 87));

        this->data->window.draw(this->pauseBtn);
        this->miniGrid->draw();
        this->view->draw();

        this->data->window.display();
    }
}
